package app;

/**
 * admin javafx application
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class AdminApplication extends AbstractApplication {

    public AdminApplication() {
        super("admin.fxml", "Admin");
    }
}
