package app;

import app.AbstractApplication;
import javafx.stage.Stage;
import util.Session;

/**
 * @author Ethan Smithweiss
 * @author Ziqi Song
 * login javafx application
 */
public class LoginApplication extends AbstractApplication {

    public LoginApplication() {
        super("login.fxml", "Login");
    }

    @Override
    public void start(Stage stage) throws Exception {
        super.start(stage);
        Session.addStage(stage);
    }

    public static void main(String[] args) {
        launch();
    }
}
