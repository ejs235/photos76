package app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.nio.file.Files;
//import java.nio.file.Path;
import java.nio.file.Paths;

import model.User;
//import model.UserList;

public class IOmanager{
	//public static final String accountStoreDir = "data";
	//public static final String accountStoreFile = "accounts.data";
	
	//private static IOmanager data;
	
	//private UserList users;
	public static final String userdir="./data/accounts";
	//private static Path userdirPath;
	//private static Path userdir= Paths.get("/data/accounts");
	/*
	public UserList getState()
	{
		return users;
	}
	public void getUser()
	{
		g
	}
	public boolean loadState() throws IOException
	{
		//if directory doesn't exist, create it
		if(!Files.exists(Paths.get(userdir)))
		{
			Files.createDirectory(Paths.get(userdir));
		}
		//if the patch isn't a directory, return false
		if(!Files.isDirectory(Paths.get(userdir)))
		{
			return false;
		}
		try(var dirstream = Files.newDirectoryStream(Paths.get(userdir)))
		{
			dirstream.forEach(path->{
				
			}
					)
			
		}
		
		
	}
	*/
	public static User readUser(String name) throws IOException, ClassNotFoundException
	{
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(userdir + File.separator + name) );
			User ret =(User)in.readObject();
			in.close();
			return ret;
	}
	public static void writeUser(User user) throws IOException, ClassNotFoundException
	{
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(userdir + File.separator + user.getName()) );
			out.writeObject(user);
			out.close();
	}
	public static boolean userExists(String name) throws IOException
	{
		if(Files.exists(Paths.get(userdir + File.separator + name)))
		{
			return true;
		}
		return false;
	}
	public static void deleteUser(String name)
	{
		 try {
			Files.deleteIfExists(Paths.get(userdir + File.separator + name));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * only for use with AdminController. do not use anywhere else
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static ArrayList<User> getUsers() throws FileNotFoundException, IOException, ClassNotFoundException
	{
		ArrayList<User> out = new ArrayList<User>();
		File folder = new File(userdir);
		for (final File fileEntry : folder.listFiles())
		{
			if (fileEntry.isFile())
			{
			ObjectInputStream currFile = new ObjectInputStream(new FileInputStream(userdir + File.separator + fileEntry.getName()) );
			out.add((User)currFile.readObject());
			currFile.close();
			}
		}
		return out;
	}
	/*
	public void LoadState()
	{
		
		try{
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(accountStoreDir + File.separator + accountStoreFile) );
			users=(UserList)in.readObject();
			in.close();
		} catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public void saveState()
	{
		try{
			ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream(accountStoreDir + File.separator + accountStoreFile));
			out.writeObject(users);
			out.close();
		} catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	*/
}