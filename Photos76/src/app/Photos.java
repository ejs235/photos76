package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
//import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import controller.LoginController;

import javafx.stage.Stage;


public class Photos extends Application {
	@Override
	public void start(Stage stage) {
		try {
			stage.setTitle("login");
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/Login.fxml"));
			VBox root = (VBox)loader.load();
			//Scene scene = new Scene(root);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			LoginController lc = loader.getController();
			lc.start(stage);
			stage.show();	
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
