package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Session;

/**
 * basic javafx application class
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public abstract class AbstractApplication extends Application {
    // fxml file
    protected String resource;
    // title
    protected String title;
    protected Stage stage;

    public AbstractApplication(String resource, String title) {
        this.resource = resource;
        this.title = title;
        stage = new Stage();
    }

    /**
     * start application
     * @throws Exception
     */
    public void start() throws Exception {
        Session.addStage(stage);
        start(stage);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource(resource));
        stage.setTitle(title);
        stage.setScene(new Scene(root));
        stage.show();
        stage.setResizable(false);
    }
}
