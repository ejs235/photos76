package controller;

import app.LoginApplication;
import app.PhotoApplication;
import app.SearchApplication;
import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Album;
import model.User;
import util.CommonUtil;
import util.FormatUtil;
import util.IOUtil;
import util.Session;

/**
 * album javafx controller
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class AlbumController {
    @FXML TableView<Album> albums;
    @FXML Button albumAdd;
    @FXML Button albumDelete;
    @FXML Button albumRename;
    @FXML Button albumOpen;
    private User user;
    void initData(User dauser)
    {
    	user = dauser;
    }
    
    
   public void addAlbum()
   {
	   
   }
   public void delAlbum()
   {
   
   }
   public void renameAlbum()
   {
	   
   }
    public void initialize() {
      
        initAlbum();
    }

    /**
     * init album list view
     */
    private void initAlbum() {
        lvAlbums.getItems().clear();
        List<Object> list = new ArrayList<>();

        String space = "    ";
        for (int i = 0; i < albums.size(); i++) {
            Album album = albums.get(i);
            int col = 0;
            GridPane albumGrid = new GridPane();
            ImageView imageView = new ImageView("album.png");
            imageView.setFitHeight(50);
            imageView.setFitWidth(50);
            albumGrid.add(imageView, col++, 0);

            albumGrid.add(new Label(space), col++, 0);

            Label name = new Label(album.getName());
            name.setFont(new Font(15));
            albumGrid.add(name, col++, 0);
            albumGrid.add(new Label(space), col++, 0);

            Label modify = new Label(FormatUtil.dateToString(album.getLastModified()));
            modify.setFont(new Font(15));
            albumGrid.add(modify, col++, 0);
            albumGrid.add(new Label(space), col++, 0);

            Button deleteBtn = new Button("Delete");
            deleteBtn.setOnMouseClicked(new AlbumDeleteHandler(album));
            deleteBtn.setFont(new Font(15));
            albumGrid.add(deleteBtn, col++, 0);
            albumGrid.add(new Label(space), col++, 0);

            Button renameBtn = new Button("Rename");
            renameBtn.setFont(new Font(15));
            albumGrid.add(renameBtn, col++, 0);
            albumGrid.add(new Label(space), col++, 0);
            renameBtn.setOnMouseClicked(new AlbumRenameHandler(album));

            Button viewBtn = new Button("Open");
            viewBtn.setFont(new Font(15));
            albumGrid.add(viewBtn, col++, 0);
            viewBtn.setOnMouseClicked(new AlbumViewHandler(album));
            list.add(albumGrid);
        }

        lvAlbums.setItems(FXCollections.observableList(list));
    }

    /**
     * create new album and refresh list
     */
    public void doCreateAlbum() {
        String name = CommonUtil.textAlert("Create Album", "Please input album name", "It must not exist");
        if (name != null && !name.isEmpty()) {
            String path = Session.currUser.getPath() + File.separator + name;
            if (IOUtil.createDir(path)) {
                Album album = new Album(Session.currUser.getName(), name, System.currentTimeMillis());
                albums.add(album);
                initAlbum();
            }
        }
    }

    /**
     * go to photo search
     * @throws Exception
     */
    public void doSearch() throws Exception {
        Session.closeStage();
        new SearchApplication().start();
    }

    /**
     * open stock album
     */
    public void doStockAlbum() {
        Session.currAlbum = Album.stockAlbum;
        Session.closeStage();
        try {
            new PhotoApplication().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * handler for album delete, it will also delete path
     */
    private class AlbumDeleteHandler implements EventHandler<MouseEvent> {
        private Album album;

        public AlbumDeleteHandler(Album album) {
            this.album = album;
        }

        @Override
        public void handle(MouseEvent event) {
            String res = CommonUtil.textAlert(
                    "Album delete",
                    "Input album name (" + album.getName() + ") to delete",
                    "This will delete all photos under this album");
            if (res != null && res.equals(album.getName())) {
                if (IOUtil.deleteDir(album.getPath())) {
                    albums.remove(album);
                    initAlbum();
                } else {
                    CommonUtil.alertError("Delete album failed");
                }
            }
        }
    }

    /**
     * handler for alnum rename, it will also rename path
     */
    private class AlbumRenameHandler implements EventHandler<MouseEvent> {
        private Album album;

        public AlbumRenameHandler(Album album) {
            this.album = album;
        }

        @Override
        public void handle(MouseEvent event) {
            String res = CommonUtil.textAlert(
                    "Album rename",
                    "Input new album name", null);
            if (res == null || res.isEmpty() || res.equals(album.getName()) || res.contains(File.separator)) {
                CommonUtil.alertError("Invalid new name");
            } else {
                if (IOUtil.renamePath(album.getPath(), album.getPath(res))) {
                    album.setName(res);
                    initAlbum();
                }
            }
        }
    }

    /**
     * handler for open album, and view photos or add photos in the album
     */
    private class AlbumViewHandler implements EventHandler<MouseEvent> {
        private Album album;

        public AlbumViewHandler(Album album) {
            this.album = album;
        }

        @Override
        public void handle(MouseEvent event) {
            Session.currAlbum = album;
            Session.closeStage();
            try {
                new PhotoApplication().start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
