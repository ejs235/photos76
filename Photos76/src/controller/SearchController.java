package controller;

import app.AlbumApplication;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.Album;
import model.Photo;
import util.CommonUtil;
import util.IOUtil;
import util.Session;

/**
 * photo search javafx controller
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class SearchController extends AbstractController {
    private final static int ROW_NUM = 4;

    @FXML
    private DatePicker dpStart;
    @FXML
    private DatePicker dpEnd;
    @FXML
    private TextField tfTag;
    @FXML
    private GridPane gpPhoto;

    private List<Photo> searchRes;
    private double itemWidth;

    /**
     * search by data range
     */
    public void doRangeSearch() {
        LocalDate start = dpStart.getValue();
        LocalDate end = dpEnd.getValue();
        if (start == null || end == null || start.isAfter(end)) {
            CommonUtil.alertError("Invalid range");
            return;
        }

        searchRes = Session.currUser.searchPhotosByTime(start, end);
        initPhotos();
    }

    /**
     * init photo grid after searching
     */
    private void initPhotos() {
        if (searchRes == null || searchRes.isEmpty()) {
            CommonUtil.alertError("No photos found");
            return;
        }
        gpPhoto.getChildren().clear();
        int row = 0;
        for (int i = 0; i < searchRes.size(); i++) {
            int col = i % ROW_NUM;
            if (i > 0 && col == 0) {
                row++;
            }
            Photo photo = searchRes.get(i);
            GridPane itemPane = new GridPane();
            itemPane.setPrefWidth(itemWidth);
            itemPane.setPrefHeight(itemWidth);
            Image image = new Image("file:" + photo.getPath());
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(itemWidth - 5);
            imageView.setFitHeight(itemWidth - 5);
            Button btn = new Button();
            btn.setPrefWidth(itemWidth - 5);
            btn.setPrefHeight(itemWidth - 5);
            btn.setGraphic(imageView);
            itemPane.add(btn, 0, 0);
            gpPhoto.add(itemPane, col, row);
        }
    }

    /**
     * search by tag
     */
    public void doTagSearch() {
        String query = tfTag.getText().trim();
        if (query.isEmpty()) {
            CommonUtil.alertError("Invalid params");
            return;
        }

        try {
            searchRes = Session.currUser.searchPhotosByTag(query);
            initPhotos();
        } catch (IllegalArgumentException e) {
            CommonUtil.alertError("Invalid params");
        }
    }

    /**
     * go back to album
     * @throws Exception
     */
    public void doGoBack() throws Exception {
        Session.closeStage();
        new AlbumApplication().start();
    }

    /**
     * save search results to another album
     */
    public void doSaveToAlbum() {
        if (searchRes == null || searchRes.isEmpty()) {
            CommonUtil.alertError("No search result");
            return;
        }
        String albumName = CommonUtil.textAlert("SaveToAlbum", "Please input album name", "New album will be create it not exist");
        if (albumName != null && !albumName.isEmpty()) {
            Album album = Session.currUser.getAlbum(albumName);
            if (album == null) {
                album = new Album(Session.currUser.getName(), albumName, System.currentTimeMillis());
                if (!IOUtil.createDir(album.getPath())) {
                    CommonUtil.alertError("Album create failed");
                }
            }

            // copy photo files and save
            for (Photo photo: searchRes) {
                if (album.photoExists(photo.getMd5())) {
                    continue;
                }

                try {
                    String photoName = photo.getName();
                    String photoPath = album.getPath() + File.separator + photo.getName();
                    if (IOUtil.fileExists(photoPath)) {
                        photoName = System.currentTimeMillis() + photoName;
                        photoPath = album.getPath() + File.separator + photoName;
                    }
                    IOUtil.copyFIle(photo.getPath(), photoPath);
                    Photo newP = new Photo(photoName, photo.getLastModified(), album.getPath(), photo.getMd5());
                    newP.save();
                    album.addPhoto(newP);
                } catch (IOException e) {
                    CommonUtil.alertError("Photo copy failed");
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        itemWidth = gpPhoto.getPrefWidth() / ROW_NUM;
        dpStart.setEditable(false);
        dpEnd.setEditable(false);
    }
}
