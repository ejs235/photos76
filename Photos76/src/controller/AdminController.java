package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import app.IOmanager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Album;
import model.User;
import util.CommonUtil;
import util.FormatUtil;
import util.IOUtil;



/**
 * admin javafx controller
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class AdminController {
    @FXML ListView<User> lvUsers;
    @FXML Button addButton;
    @FXML Button remButton;
    private static ObservableList<User> users;

    private LoginController loginController;
    
    public void setLoginController(LoginController controller)
    {
    	loginController=controller;
    }
    /**
     * load user list when init
     * @param location
     * @param resources
     */
    public void initialize() {
        try {
            try {
				users = FXCollections.observableArrayList(IOmanager.getUsers());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            System.out.println(users);
            //initUsers();
        } catch (IOException e) {
            e.printStackTrace();
            CommonUtil.alertError("load users failed, please check path");
            System.exit(1);
        }
        lvUsers.setCellFactory(lv -> new ListCell<User>(){
			@Override
			public void updateItem(User user, boolean empty) {
				super.updateItem(user,empty);
				setText(empty ? null : user.getName());
			}
		});
    lvUsers.setItems(users);
	lvUsers.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	// select the first item
	lvUsers.getSelectionModel().select(0);
    }
    
    public void doDeleteUser()
    {
    	if(CommonUtil.confirmAlert("User Delete","","delete user "+lvUsers.getSelectionModel().getSelectedItem().getName()+"?"))
    	{
    		IOmanager.deleteUser(lvUsers.getSelectionModel().getSelectedItem().getName());
			lvUsers.getItems().remove(lvUsers.getSelectionModel().getSelectedIndex());
			lvUsers.getSelectionModel().clearSelection();
    	}
    }
    
    public void doAddUser() {
        String res = CommonUtil.textAlert(
                "User Create",
                "Input user name",
                "User name can't be duplicate");
        if (res != null && !res.isEmpty() && !res.contains(File.separator) && !res.equals("admin")) {
            for (User user: users) {
                if (user.getName().equals(res)) {
                    CommonUtil.alertError("User exists");
                    return;
                }
            }

            //User user = new User(false, res);
            User user = new User(res);
           try{
				IOmanager.writeUser(user);
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            //IOUtil.createDir(user.getPath());
           // users.add(user);
           lvUsers.getItems().add(user);
           lvUsers.getSelectionModel().clearSelection();
           lvUsers.getSelectionModel().selectLast();
        }
        CommonUtil.alertError("User exists");
    }
    /**
     * init user list view
     */
    /*
    private void initUsers() {
        lvUsers.getItems().clear();
        List<Object> list = new ArrayList<>();

        String space = "    ";
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            int col = 0;
            GridPane userGrid = new GridPane();
            ImageView imageView = new ImageView("user.png");
            imageView.setFitHeight(50);
            imageView.setFitWidth(50);
            userGrid.add(imageView, col++, 0);

            userGrid.add(new Label(space), col++, 0);

            Label name = new Label(user.getName());
            name.setFont(new Font(15));
            userGrid.add(name, col++, 0);
            userGrid.add(new Label(space), col++, 0);

            Button deleteBtn = new Button("Delete");
            deleteBtn.setOnMouseClicked(new UserDeleteHandler(user));
            deleteBtn.setFont(new Font(15));
            userGrid.add(deleteBtn, col++, 0);

            list.add(userGrid);
        }

        lvUsers.setItems(FXCollections.observableList(list));
    }

   
    
     public void doCreateUser() {
        String res = CommonUtil.textAlert(
                "User Create",
                "Input user name",
                "User name can't be duplicate");
        if (res != null && !res.isEmpty() && !res.contains(File.separator)) {
            for (User user: users) {
                if (user.getName().equals(res)) {
                    CommonUtil.alertError("User exists");
                    return;
                }
            }

            //User user = new User(false, res);
            User user = new User(res);
           try{
				IOmanager.writeUser(user);
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            //IOUtil.createDir(user.getPath());
            users.add(user);
            initUsers();
        }
    }
    
    public void deleteUser()
    {
    	String res = CommonUtil.textAlert(
                "User Delete",
                "Input user name",
                "User name must exist");
        if (res != null && !res.isEmpty() && !res.contains(File.separator)) {
            if(!IOmanager.userExists(res))
            {
            	CommonUtil.alertError("User does not exist");
            	return;
            }
            }

            //User user = new User(false, res);
            User user = new User(res);
            try {
				IOmanager.writeUser(user);
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            //IOUtil.createDir(user.getPath());
            users.add(user);
            initUsers();
        }
    }
 
    class UserDeleteHandler implements EventHandler<MouseEvent> {
        private User user;

        public UserDeleteHandler(User user) {
            this.user = user;
        }

        @Override
        public void handle(MouseEvent event) {
            String res = CommonUtil.textAlert(
                    "User delete",
                    "Input user name (" + user.getName() + ") to delete",
                    "This will delete all photos of this user");
            if (res != null && res.equals(user.getName())) {
                if (IOUtil.deleteDir(user.getPath())) {
                    users.remove(user);
                    initUsers();
                } else {
                    CommonUtil.alertError("Delete album failed");
                }
            }
            IOUtil.deleteDir(user.getPath());
        }
    }
    */
}
