package controller;

import app.AlbumApplication;
import app.ViewPhotoApplication;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import model.Photo;
import util.Session;

/**
 * photos javafx controller, to view or add photos inside an album
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class PhotoController extends AbstractController {
    private final static int ROW_NUM = 4;

    @FXML
    private Label lbAlbum;
    @FXML
    private GridPane gpPhoto;
    @FXML
    private Button btnAddPhoto;

    private List<Photo> photos;

    private double itemWidth;

    /**
     * load photos of current album
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (Session.isStockAlbum()) {
            btnAddPhoto.setVisible(false);
        }
        Session.currAlbum.loadPhotos();
        photos = Session.currAlbum.getPhotos();

        lbAlbum.setText(Session.currAlbum.getName());

        itemWidth = gpPhoto.getPrefWidth() / ROW_NUM;

        initPhotos();
    }

    /**
     * init photos grid
     */
    private void initPhotos() {
        gpPhoto.getChildren().clear();
        int row = 0;
        for (int i = 0; i < photos.size(); i++) {
            int col = i % ROW_NUM;
            if (i > 0 && col == 0) {
                row++;
            }
            Photo photo = photos.get(i);
            GridPane itemPane = new GridPane();
            itemPane.setPrefWidth(itemWidth);
            itemPane.setPrefHeight(itemWidth);
            Image image = new Image("file:" + photo.getPath());
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth(itemWidth - 5);
            imageView.setFitHeight(itemWidth - 5);
            Button btn = new Button();
            btn.setPrefWidth(itemWidth - 5);
            btn.setPrefHeight(itemWidth - 5);
            btn.setGraphic(imageView);
            btn.setOnMouseClicked(new ImageViewHandler(photo));
            itemPane.add(btn, 0, 0);
            gpPhoto.add(itemPane, col, row);
        }
    }

    /**
     * go back to album
     * @throws Exception
     */
    public void doGoBack() throws Exception {
        Session.currAlbum = null;
        Session.closeStage();
        new AlbumApplication().start();
    }

    /**
     * add photo
     * @throws Exception
     */
    public void doAddPhoto() throws Exception {
        Session.closeStage();
        Session.currPhoto = null;
        new ViewPhotoApplication().start();
    }

    /**
     * show view detail info
     */
    private class ImageViewHandler implements EventHandler<MouseEvent> {
        private Photo photo;

        public ImageViewHandler(Photo photo) {
            this.photo = photo;
        }

        @Override
        public void handle(MouseEvent event) {
            Session.currPhoto = photo;
            Session.closeStage();
            try {
                new ViewPhotoApplication().start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
