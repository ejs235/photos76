package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.User;
import app.IOmanager;


public class LoginController{
	
	@FXML Button loginButton;
	
	@FXML TextField loginPrompt;
	
	@FXML Text errorText;
	public void start(Stage mainstage){
		mainstage.setTitle("login");	

		
		mainstage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we) {
				mainstage.close();
			}
		});
		
	}
	public void doLogin()
	{
		System.out.printf("oyoyo\n");
		//if user is admin, load admin application
		if(!new File(IOmanager.userdir).exists())
		{
			errorText.setText("user directory is invalid");
		}
		else if(loginPrompt.getText().equals("admin"))
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Admin.fxml"));
			Stage stage = new Stage();
			try {
				stage.setScene(new Scene(loader.load()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			AdminController controller = loader.getController();
			controller.initialize();
			 stage.show();
		}
		//if user is not admin, if they exist, load the albums controller for them.
		else{
			try {
				if(IOmanager.userExists(loginPrompt.getText()))
				{
					
				}
				//else, print an error
				else
				{
					errorText.setText("user does not exist");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}
}