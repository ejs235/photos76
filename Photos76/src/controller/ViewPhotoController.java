package controller;

import app.AlbumApplication;
import app.PhotoApplication;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import model.Album;
import model.Photo;
import model.Tag;
import util.CommonUtil;
import util.FormatUtil;
import util.IOUtil;
import util.Session;

/**
 * view photo or add photo javafx controller
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class ViewPhotoController extends AbstractController {
    @FXML
    private Label lbTitle;
    @FXML
    private ImageView ivPhoto;
    @FXML
    private Button btnChooseNew;
    @FXML
    private Button btnSubmit;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnUpdate;
    @FXML
    private TextField tfCaption;
    @FXML
    private TextField tfValue;
    @FXML
    private Label lbDate;
    @FXML
    private ListView lvTags;
    @FXML
    private ComboBox<String> cbTag;
    @FXML
    private Button btnCopy;

    private File newImageFile;

    private Photo photo;
    private List<Tag> tags;

    /**
     * init views by judge photo in session
     * it means view photo detail if Session.currPhoto is not null, otherwise create new
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        photo = Session.currPhoto;
        cbTag.setEditable(false);
        if (photo != null) {
            lbTitle.setText("View Photo");
            btnSubmit.setVisible(false);
            btnChooseNew.setVisible(false);
            lbDate.setText(FormatUtil.dateToString(photo.getLastModified()));
            tfCaption.setText(photo.getCaption());
            tags = photo.getTags();
            Image image = new Image("file:" + photo.getPath());
            ivPhoto.setImage(image);
        } else {
            lbDate.setText("");
            lbTitle.setText("New Photo");
            btnDelete.setVisible(false);
            btnUpdate.setVisible(false);
            tags = new ArrayList<>();
            btnCopy.setVisible(false);
        }
        if (Session.isStockAlbum()) {
            btnDelete.setVisible(false);
            btnUpdate.setVisible(false);
            tfCaption.setEditable(false);
            tfValue.setEditable(false);
        }

        cbTag.setItems(FXCollections.observableArrayList("Location", "Person"));

        initTags();
    }

    /**
     * init tag list
     */
    private void initTags() {
        lvTags.getItems().clear();
        List<Object> list = new ArrayList<>();

        String space = "    ";
        for (int i = 0; i < tags.size(); i++) {
            Tag tag = tags.get(i);
            int col = 0;
            GridPane tagGrid = new GridPane();
            Label tagLabel = new Label(tag.toString());
            tagLabel.setFont(new Font(15));
            tagGrid.add(tagLabel, col++, 0);
            tagGrid.add(new Label(space), col++, 0);

            Button deleteBtn = new Button("Delete");
            deleteBtn.setOnMouseClicked(new TagDeleteHandler(i));
            deleteBtn.setFont(new Font(15));
            tagGrid.add(deleteBtn, col++, 0);

            list.add(tagGrid);
        }

        lvTags.setItems(FXCollections.observableList(list));
    }

    /**
     * choose an image file to add
     */
    public void doChooseNewImage() {
        newImageFile = IOUtil.chooseAndUpdateImageView(ivPhoto);
        if (newImageFile != null) {
            lbDate.setText(FormatUtil.dateToString(FormatUtil.timeToDate(newImageFile.lastModified())));
        }
    }

    /**
     * add tag
     */
    public void doAddTag() {
        if (Session.isStockAlbum()) {
            CommonUtil.alertError("Can't modify stock");
            return;
        }

        String key = cbTag.getValue();
        String value = tfValue.getText().trim();
        if (value.isEmpty()) {
            CommonUtil.alertError("Value is empty");
        } else {
            if (Tag.addTag(tags, key, value)) {
                initTags();
            } else {
                CommonUtil.alertError("Add failed, tips: there can only be one location");
            }
        }
    }

    /**
     * close
     * @throws Exception
     */
    public void doClose() throws Exception {
        Session.currPhoto = null;
        Session.closeStage();
        new PhotoApplication().start();
    }

    /**
     * submit new photo
     * @throws Exception
     */
    public void doSubmit() throws Exception {
        if (newImageFile == null) {
            CommonUtil.alertError("Please choose an photo");
            return;
        }
        String md5 = IOUtil.getFileMD5(newImageFile.getAbsolutePath());
        if (Session.currAlbum.photoExists(md5)) {
            CommonUtil.alertError("Photo already exists");
            return;
        }

        String caption = tfCaption.getText().trim();
        String albumPath = Session.currAlbum.getPath();
        String name = newImageFile.getName();
        String path = albumPath + File.separator + name;
        if (IOUtil.fileExists(path)) {
            name = System.currentTimeMillis() + name;
            path = albumPath + File.separator + name;
        }
        try {
            IOUtil.copyFIle(newImageFile, new File(path));
        } catch (IOException e) {
            e.printStackTrace();
            CommonUtil.alertError("Image copy failed");
        }

        Photo photo = new Photo(name, newImageFile.lastModified(), albumPath, md5);
        photo.setCaption(caption);
        Session.currAlbum.addPhoto(photo);
        photo.save();
        doClose();
    }

    /**
     * delete one photo
     * @throws Exception
     */
    public void doDelete() throws Exception {
        if (Session.isStockAlbum()) {
            CommonUtil.alertError("Can't modify stock");
            return;
        }

        boolean res = CommonUtil.confirmAlert(
                "Photo delete",
                "Are you sure to delete the photo",
                "All info of this photo will be delete");
        if (res) {
            photo.getPath();
            if (IOUtil.deletePath(new File(photo.getPath() + IOUtil.DATA_PREFIX))
                    && IOUtil.deletePath(new File(photo.getPath()))) {
                doClose();
            } else {
                CommonUtil.alertError("Delete album failed");
            }
        }
    }

    /**
     * update current photo info
     * @throws Exception
     */
    public void doUpdate() throws Exception {
        if (Session.isStockAlbum()) {
            CommonUtil.alertError("Can't modify stock");
            return;
        }

        String caption = tfCaption.getText().trim();
        photo.setCaption(caption);
        photo.save();
        doClose();
    }

    /**
     * copy photo to another album
     */
    public void doCopy() {
        String name = CommonUtil.textAlert("Copy to another album", "Input another name of album", "The album must exist");
        if (name == null || name.isEmpty() || name.equals(Session.currAlbum.getName())) {
            CommonUtil.alertError("Name is empty or equal to current album");
        } else {
            Album album = Session.currUser.getAlbum(name);
            if (album == null) {
                CommonUtil.alertError("Album not exist");
            } else {
                if (album.photoExists(photo.getMd5())) {
                    CommonUtil.alertError("Photo already exist");
                    return;
                }
                Photo copy = new Photo(photo.getName(), photo.getLastModified(), album.getPath(), photo.getMd5());
                try {
                    IOUtil.copyFIle(photo.getPath(), copy.getPath());
                    copy.save();
                    album.addPhoto(copy);
                    CommonUtil.alertInfo("Photo copy success");
                } catch (IOException e) {
                    e.printStackTrace();
                    CommonUtil.alertError("Photo copy failed");
                }
            }
        }
    }

    /**
     * handler for tag deletion
     */
    private class TagDeleteHandler implements EventHandler<MouseEvent> {
        private int idx;

        public TagDeleteHandler(int idx) {
            this.idx = idx;
        }

        @Override
        public void handle(MouseEvent event) {
            tags.remove(idx);
            lvTags.getItems().remove(idx);
        }
    }
}
