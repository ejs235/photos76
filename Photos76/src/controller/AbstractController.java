package controller;

import app.LoginApplication;
import javafx.fxml.Initializable;
import util.Session;

/**
 * basic javafx controller
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public abstract class AbstractController implements Initializable {

    /**
     * logout
     * @throws Exception
     */
    public void doLogout() throws Exception {
        Session.clearData();
        Session.closeStage();
        new LoginApplication().start();
    }

    /**
     * exit
     */
    public void doExit() {
        System.exit(0);
    }
}
