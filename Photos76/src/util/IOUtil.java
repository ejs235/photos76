package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Photo;

/**
 * io utils
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class IOUtil {
    public final static String DATA_PREFIX = ".info";

    /**
     * get user albums by check dir in path
     * @param path
     * @return
     * @throws IOException
     */
    public static List<File> getUserAlbums(String path) throws IOException {
        File dir = new File(path);
        if (dir.exists() && dir.isFile()) {
            throw new IOException("Path is a file");
        } else if (!dir.exists()) {
            dir.mkdirs();
        }

        File[] subDirs = dir.listFiles();
        List<File> albums = new ArrayList<>();
        for (File subDir: subDirs) {
            if (subDir.isDirectory()) {
                albums.add(subDir);
            }
        }

        return albums;
    }


    /**
     * check if directory is exist
     * @param path
     * @return
     */
    public static boolean isDirectoryExist(String path) {
        File dir = new File(path);
        return dir.exists() && dir.isDirectory();
    }

    /**
     * write object to file
     * @param photo
     * @param filePath
     * @return
     */
    public static boolean writeObjectToFile(Photo photo, String filePath) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath + DATA_PREFIX))) {
            oos.writeObject(photo);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * read object from file
     * @param file
     * @return
     */
    public static Object readObjectFromFile(File file) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            return ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * list dirs in path
     * @param path
     * @return
     */
    public static List<File> listDirs(String path) {
        File dir = new File(path);
        if (!dir.exists() || dir.isFile()) {
            return null;
        }

        List<File> files = new ArrayList<>();
        for (File file: dir.listFiles()) {
            if (file.isDirectory()) {
                files.add(file);
            }
        }

        return files;
    }

    /**
     * check file exists
     * @param filePath
     * @return
     */
    public static boolean fileExists(Path filePath) {
        File file = new File(filePath);
        return file.exists() && file.isFile();
    }

    /**
     * read photo infos from path
     * @param filePath
     * @return
     */
    public static List<Photo> readPhotos(String filePath) {
        File dir = new File(filePath);
        File[] files = dir.listFiles((dir1, name) -> name.endsWith(DATA_PREFIX));

        List<Photo> photos = new ArrayList<>();
        for (File file: files) {
            Object obj = readObjectFromFile(file);
            if (obj != null) {
                Photo photo = (Photo) obj;
                if (fileExists(photo.getPath())) {
                    photos.add(photo);
                }
            }
        }

        return photos;
    }

    /**
     * recursive delete path
     * @param path
     * @return
     */
    public static boolean deleteDir(String path) {
        File file = new File(path);
        if (file.exists() && file.isFile()) {
            return false;
        }

        return deletePath(file);
    }

    /**
     * recursive delete path
     * @param file
     * @return
     */
    public static boolean deletePath(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File file1 : files) {
                    if (!deletePath(file1)) {
                        return false;
                    }
                }
                return file.delete();
            } else {
                return file.delete();
            }
        }

        return true;
    }

    /**
     * rename path
     * @param from
     * @param to
     * @return
     */
    public static boolean renamePath(String from, String to) {
        File file = new File(from);
        if (file.exists()) {
            return file.renameTo(new File(to));
        }

        return false;
    }

    /**
     * create directory
     * @param path
     * @return
     */
    public static boolean createDir(String path) {
        File dir = new File(path);
        if (dir.exists()) {
            return false;
        }

        return dir.mkdir();
    }

    /**
     * copy file
     * @param source
     * @param dest
     * @throws IOException
     */
    public static void copyFIle(String source, String dest) throws IOException {
        copyFIle(new File(source), new File(dest));
    }

    /**
     * copy file
     * @param source
     * @param dest
     * @throws IOException
     */
    public static void copyFIle(File source, File dest)
            throws IOException {
        Files.copy(source.toPath(), dest.toPath());
    }

    /**
     * select image file
     * @return
     */
    public static FileChooser getImageFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Image File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("BMP", "*.bmp"),
                new FileChooser.ExtensionFilter("GIF", "*.gif")
        );

        return fileChooser;
    }

    /**
     * select image and update image view
     * @param imageView
     * @return
     */
    public static File chooseAndUpdateImageView(ImageView imageView) {
        File imageFile = getImageFileChooser().showOpenDialog(new Stage());
        if (imageFile != null && imageFile.exists()) {
            try {
                Image image = new Image("file:" + imageFile.getAbsolutePath());
                imageView.setImage(image);
            } catch (Exception e) {
                e.printStackTrace();
                new Alert(AlertType.ERROR, "image preview failed");
            }
        }

        return imageFile;
    }

    /**
     * get file md5
     * @param path
     * @return
     */
    public static String getFileMD5(String path) {
        File file = new File(path);
        FileInputStream fileInputStream = null;
        try {
            MessageDigest MD5 = MessageDigest.getInstance("MD5");
            fileInputStream = new FileInputStream(file);
            byte[] buffer = new byte[8192];
            int length;
            while ((length = fileInputStream.read(buffer)) != -1) {
                MD5.update(buffer, 0, length);
            }

            return new String(MD5.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (fileInputStream != null){
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
