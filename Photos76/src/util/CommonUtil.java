package util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

/**
 * common utils
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class CommonUtil {

    /**
     * alert error
     * @param msg
     */
    public static void alertError(String msg) {
        Alert alert = new Alert(AlertType.ERROR, msg);
        alert.show();
    }

    /**
     * alert info
     * @param msg
     */
    public static void alertInfo(String msg) {
        Alert alert = new Alert(AlertType.INFORMATION, msg);
        alert.show();
    }

    /**
     * show a confirm dialog
     * @param title
     * @param msg
     * @param header
     * @return
     */
    public static boolean confirmAlert(String title, String msg, String header) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(msg);

        Optional<ButtonType> res = alert.showAndWait();
        return res.get() == ButtonType.OK;
    }

    /**
     * show a text dialog
     * @param title
     * @param msg
     * @param header
     * @return
     */
    public static String textAlert(String title, String msg, String header) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setContentText(msg);
        if (header != null) {
            dialog.setHeaderText(header);
        }

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()){
            return result.get();
        }

        return null;
    }

    /**
     * check data range
     * @param start
     * @param end
     * @param target
     * @return
     */
    public static boolean dateInRange(LocalDateTime start, LocalDateTime end, LocalDateTime target) {
        if (target.isBefore(start) || target.isAfter(end)) {
            return false;
        }

        return true;
    }

}
