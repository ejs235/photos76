package util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * util to format
 * @author Ethan Smithweiss
 * @author Ziqi Song
 */
public class FormatUtil {

    /**
     * convert ldt to string
     * @param date
     * @return
     */
    public static String dateToString(LocalDateTime date) {
        return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
    }

    /**
     * convert ldt to timestamp
     * @param date
     * @return
     */
    public static long localDateTimeToTimestamp(LocalDateTime date) {
        return date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * convert timestamp to ldt
     * @param timestamp
     * @return
     */
    public static LocalDateTime timeToDate(long timestamp) {
        return Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
