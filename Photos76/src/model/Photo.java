package model;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;
//import java.util.UUID;

public class Photo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Path path;
	//private UUID photoID;
	//private int tagCount;
	public ArrayList<Tag> tagList;
	private int linkCount;
	private LocalDateTime pdate;
	private String caption;
	
	public Photo(Path npath, LocalDateTime date)
	{
		path=npath;
		pdate=date;

		linkCount=1;
		caption="";
		tagList = new ArrayList<Tag>();
	}
	public LocalDateTime getDate()
	{
		return pdate;
	}
	public Path getPath()
	{
		return path;
	}
	public void addTag(Tag t)
	{
		tagList.add(t);
	}
	public void remTag(int index)
	{
		tagList.remove(index);
	}
	public Tag getTag(int index)
	{
		return tagList.get(index);
	}
	public boolean hasTag(Tag t)
	{
		for(Tag g: tagList)
		{
			if(t.equals(g))
			{
				return true;
			}
		}
		return false;
	}
	public int getTagCount()
	{
		return tagList.size();
	}
	public int getLinkCount()
	{
		return linkCount;
	}
	public void incLinks()
	{
		linkCount++;
	}
	public void decLinks()
	{
		linkCount--;
	}
	public void setCaption(String s)
	{
		caption=s;
	}
	public String getCaption()
	{
		return caption;
	}
}