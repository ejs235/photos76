package model;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ListIterator;

public class Album implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	public ArrayList<Path> photoList = new ArrayList<Path>();
	private PhotoMap map;
	public Album(String daname,PhotoMap damap)
	{
		name=daname;
		map=damap;
	}
	public int getSize()
	{
		return photoList.size();
	}
	public LocalDateTime GetOldestDate()
	{
		if(photoList.size() < 1)
		{
			return null;
		}
		LocalDateTime ret = map.getDate(photoList.get(0));
		ListIterator<Path>  liter= photoList.listIterator(1);
		while(liter.hasNext())
		{
			LocalDateTime curr=map.getDate(liter.next());
			if(curr.isBefore(ret))
			{
				ret=curr;
			}
		}
		return ret;
	}
	public LocalDateTime GetNewestDate()
	{
		if(photoList.size() < 1)
		{
			return null;
		}
		LocalDateTime ret = map.getDate(photoList.get(0));
		ListIterator<Path>  liter= photoList.listIterator(1);
		while(liter.hasNext())
		{
			LocalDateTime curr=map.getDate(liter.next());
			if(curr.isAfter(ret))
			{
				ret=curr;
			}
		}
		return ret;
	}
	public boolean hasPhoto(Path path)
	{
		return photoList.contains(path);
	}
	public void addPhoto(Path path, LocalDateTime date)
	{
		photoList.add(path);
		map.linkPhoto(path,date);
	}
	public void remPhoto(Path path)
	{
		map.unlinkPhoto(path);
		photoList.remove(path);
	}
	public String getName()
	{
		return name;
	}
	public void setName(String s)
	{
		name=s;
	}
	
	/**
	 * removes all songs from the album 
	 * used when deleting an album
	 */
	public void empty()
	{
		ListIterator<Path> liter= photoList.listIterator();
		while(liter.hasNext())
		{
			remPhoto(liter.next());
		}
	}
}