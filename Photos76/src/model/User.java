package model;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;
//import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;


public class User implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PhotoMap map;
	private Path path;
	private String name;
	//private ArrayList<Album> albums;
	public Map<String,Album> albums;
	public User(String daname)
	{
		name=daname;
		map=new PhotoMap();
		//albums = new ArrayList<Album>();
		albums = new HashMap<String,Album>();
	}
	public void makeAlbum(String albname)
	{
		Album newAlbum= new Album(albname,map);
		albums.put(albname,newAlbum);
	}
	public void delAlbum(String albname)
	{
		albums.get(albname).empty();
		albums.remove(albname);
	}
	
	public String getName()
	{
		return name;
	}
	public void setName(String s)
	{
		name=s;
	}
	public Album getAlbum(String name) {
		return albums.get(name);
       }
	public ArrayList<Photo> searchBefore( LocalDateTime latest)
	{
		return map.searchBefore(latest);
	}
	public ArrayList<Photo> searchAfter( LocalDateTime earliest)
	{
		 return map.searchAfter(earliest);
	}
	public ArrayList<Photo> searchTag(Tag t)
	{
		return map.searchTag(t);
	}
}