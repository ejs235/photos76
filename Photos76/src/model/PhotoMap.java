package model;


import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;



public class PhotoMap implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * maps each photo to its path
	 */
	private Map<Path,Photo> photoMap;
	public PhotoMap()
	{
		photoMap = new HashMap<Path,Photo>();
	} 
	/*
	private boolean checkDupPath(String path)
	{
		
		return true;
	}*/
	/**
	 * gets date of given photo
	 * @param path
	 * @return
	 */
	public LocalDateTime getDate(Path path)
	{
		return photoMap.get(path).getDate();
	}
	/**
	 * states whether a given photo exists
	 * @param path
	 * @return
	 */
	public boolean hasPhoto(Path path)
	{
		return photoMap.containsKey(path);
	}
	/**
	 * adds a photo to the list if it doesn't already exist,
	 * otherwise increments the number of links to it from albums
	 * @param path
	 * @param date
	 */
	public void linkPhoto(Path path, LocalDateTime date)
	{
		if(photoMap.containsKey(path))
		{
			photoMap.get(path).incLinks();
		}
		else
		{
			Photo new_photo = new Photo(path,date);
			photoMap.put(path,new_photo);
		}
		//check if path matches. if it does, just increment the link.
		//otherwise make new entry in map
	}
	/**
	 * decrements the link count of a photo,
	 * removes if from the list if the linkcount reaches 0
	 * @param path
	 */
	public void unlinkPhoto(Path path)
	{
		photoMap.get(path).decLinks();
		//check if link
		if(photoMap.get(path).getLinkCount()<1)
		{
			photoMap.remove(path);
		}
	}
	/**
	 * returns a list of photos that who's dates precede a given date
	 * @param latest
	 * @return
	 */
	public ArrayList<Photo> searchBefore( LocalDateTime latest)
	{
		 ArrayList<Photo> out = new  ArrayList<Photo>();
		for(Map.Entry<Path,Photo> m: photoMap.entrySet()){
			if(((Photo) m.getValue()).getDate().isBefore(latest))
			{
				out.add(((Photo) m.getValue()));
			}
		}
		return out;
	}
	/**
	 * returns a list of photos whose dates succeed a given date
	 * @param earliest
	 * @return
	 */
	public ArrayList<Photo> searchAfter( LocalDateTime earliest)
	{
		 ArrayList<Photo> out = new  ArrayList<Photo>();
		for(Map.Entry<Path,Photo> m: photoMap.entrySet()){
			if(((Photo) m.getValue()).getDate().isAfter(earliest))
			{
				out.add(((Photo) m.getValue()));
			}
		}
		return out;
	}
	
	public ArrayList<Photo> searchTag(Tag t)
	{
		ArrayList<Photo> out = new  ArrayList<Photo>();
		for(Map.Entry<Path,Photo> m: photoMap.entrySet()){
			if(((Photo) m.getValue()).hasTag(t))
			{
				out.add(((Photo) m.getValue()));
			}
		}
		return out;
	}
	
}