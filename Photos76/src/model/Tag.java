package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Tag implements Serializable{


	private static final long serialVersionUID = 1L;
	public String type;
	public ArrayList<String> fields;
	
	public Tag(String typen, int fieldCount, ArrayList<String> fieldsn)
	{
		type=typen;
		fields = new ArrayList<String>();
		fields = (ArrayList<String>)fieldsn.clone();
	}
	
	public boolean hasField(String daField)
	{
		return fields.contains(daField);
	}
	public String getType()
	{
		return type;
	}
	public ArrayList<String> getFields()
	{
		return fields;
	}
	public boolean equals(Object o)
	{
		if(this == o)
		{
			return true;
		}
		if(o instanceof Tag)
		{
			Tag t = (Tag)o;
			if(!this.getType().equals(t.getType()))
			{
				return false;
			}
			if(this.fields.size()!=t.getFields().size())
			{
				return false;
			}
			Iterator<String> i1=this.fields.iterator();
			Iterator<String> i2=t.getFields().iterator();
			while(i1.hasNext() && i2.hasNext())
			{
				if(!i1.next().equals(i2.next()))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
}